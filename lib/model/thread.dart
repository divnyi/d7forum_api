import 'package:d7forum_api/utils/utils.dart';

import 'user.dart';
import 'forum.dart';

class Thread {
  /// can_edit
  bool canEdit;
  /// can_edit_tags
  bool canEditTags;
  /// can_hard_delete
  bool canHardDelete;
  /// can_reply
  bool canReply;
  /// can_soft_delete
  bool canSoftDelete;
  /// can_view_attachments
  bool canViewAttachments;
  /// custom_fields
  Json customFields;
  /// discussion_open
  bool discussionOpen;
  /// discussion_state
  String discussionState;
  /// discussion_type
  String discussionType;
  /// first_post_id
  int firstPostId;
  /// first_post_reaction_score
  int firstPostReactionScore;
  /// last_post_date
  DateTime lastPostDate;
  /// last_post_id
  int lastPostId;
  /// last_post_user_id
  int lastPostUserId;
  /// last_post_username
  String lastPostUsername;
  /// node_id
  int nodeId;
  /// post_date
  DateTime postDate;
  /// prefix_id
  int prefixId;
  /// prefix
  Json prefix;
  /// reply_count
  int replyCount;
  /// sticky
  bool sticky;
  /// tags
  Json tags;
  /// thread_id
  int threadId;
  /// title
  String title;
  /// view_count
  int viewCount;
  /// `is_watching`
  bool isWatching;
  /// `visitor_post_count`
  int visitorPostCount;

  /// User
  User user;
  /// Forum
  Forum forum;

  Thread({
    this.canEdit,
    this.canEditTags,
    this.canHardDelete,
    this.canReply,
    this.canSoftDelete,
    this.canViewAttachments,
    this.customFields,
    this.discussionOpen,
    this.discussionState,
    this.discussionType,
    this.firstPostId,
    this.firstPostReactionScore,
    this.lastPostDate,
    this.lastPostId,
    this.lastPostUserId,
    this.lastPostUsername,
    this.nodeId,
    this.postDate,
    this.prefixId,
    this.prefix,
    this.replyCount,
    this.sticky,
    this.tags,
    this.threadId,
    this.title,
    this.viewCount,
    this.user,
    this.isWatching,
    this.forum,
  });

  static final errorIsDamaged = D7ForumApiException("thread.isDamaged() == true");
  Thread.fromWeb(Json j) {
    this.canEdit = j.path("can_edit").guess();
    this.canEditTags = j.path("can_edit_tags").guess();
    this.canHardDelete = j.path("can_hard_delete").guess();
    this.canReply = j.path("can_reply").guess();
    this.canSoftDelete = j.path("can_soft_delete").guess();
    this.canViewAttachments = j.path("can_view_attachments").guess();
    this.customFields = j.path("custom_fields").guess();
    this.discussionOpen = j.path("discussion_open").guess();
    this.discussionState = j.path("discussion_state").guess();
    this.discussionType = j.path("discussion_type").guess();
    this.firstPostId = j.path("first_post_id").guess();
    this.firstPostReactionScore = j.path("first_post_reaction_score").guess();
    this.lastPostDate = j.path("last_post_date").guess();
    this.lastPostId = j.path("last_post_id").guess();
    this.lastPostUserId = j.path("last_post_user_id").guess();
    this.lastPostUsername = j.path("last_post_username").guess();
    this.nodeId = j.path("node_id").guess();
    this.postDate = j.path("post_date").guess();
    this.prefixId = j.path("prefix_id").guess();
    this.prefix = j.path("prefix").guess();
    this.replyCount = j.path("reply_count").guess();
    this.sticky = j.path("sticky").guess();
    this.tags = j.path("tags").guess();
    this.threadId = j.path("thread_id").guess();
    this.title = j.path("title").guess();
    this.viewCount = j.path("view_count").guess();
    this.isWatching = j.path("is_watching").guess() ?? false;
    this.visitorPostCount = j.path("visitor_post_count").guess();


    if (this.isDamaged()) {
      errorIsDamaged.throwError(json: j);
    }

    this.user = User.fromWeb(j.path("User"));

    try {
      this.forum = Forum.fromWeb(j.path("Forum"));
    } catch (e) { }
  }

  bool isDamaged() {
    return this.canEdit == null
        || this.canEditTags == null
        || this.canHardDelete == null
        || this.canReply == null
        || this.canSoftDelete == null
        || this.canViewAttachments == null
        || this.customFields == null
        || this.discussionOpen == null
        || this.discussionState == null
        || this.discussionType == null
        || this.firstPostId == null
        || this.firstPostReactionScore == null
        || this.lastPostDate == null
        || this.lastPostId == null
        || this.lastPostUserId == null
        || this.lastPostUsername == null
        || this.nodeId == null
        || this.postDate == null
        || this.prefixId == null
        || this.replyCount == null
        || this.sticky == null
        || this.tags == null
        || this.threadId == null
        || this.title == null
        || this.viewCount == null;
  }

  @override
  bool operator ==(other) {
    return this.threadId == other.threadId;
  }
  @override
  int get hashCode => threadId.hashCode;
}