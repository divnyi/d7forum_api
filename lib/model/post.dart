import 'package:d7forum_api/utils/utils.dart';

import 'user.dart';
import 'attachment.dart';
import 'thread.dart';

class Post {
  /// attach_count
  int attachCount;
  /// can_edit
  bool canEdit;
  /// can_hard_delete
  bool canHardDelete;
  /// can_react
  bool canReact;
  /// can_soft_delete
  bool canSoftDelete;
  /// can_view_attachments
  bool canViewAttachments;
  /// is_first_post
  bool isFirstPost;
  /// is_last_post
  bool isLastPost;
  /// last_edit_date
  int lastEditDate;
  /// message
  String message;
  /// message_state
  String messageState;
  /// position
  int position;
  /// post_date
  DateTime postDate;
  /// post_id
  int postId;
  /// reaction_score
  int reactionScore;
  /// thread_id
  int threadId;
  /// warning_message
  String warningMessage;
  /// `is_reacted_to`
  bool isReactedTo;
  /// `visitor_reaction_id`
  int visitorReactionId;

  /// User
  User user;
  /// Thread
  Thread thread;
  /// Attachments
  List<Attachment> attachments;

  Post({
    this.attachCount,
    this.canEdit,
    this.canHardDelete,
    this.canReact,
    this.canSoftDelete,
    this.canViewAttachments,
    this.isFirstPost,
    this.isLastPost,
    this.lastEditDate,
    this.message,
    this.messageState,
    this.position,
    this.postDate,
    this.postId,
    this.reactionScore,
    this.threadId,
    this.warningMessage,
    this.isReactedTo,
    this.visitorReactionId,
    this.user,
    this.attachments,
    this.thread,
  });

  static final errorIsDamaged = D7ForumApiException("post.isDamaged() == true");
  Post.fromWeb(Json j) {
    this.attachCount = j.path("attach_count").guess();
    this.canEdit = j.path("can_edit").guess();
    this.canHardDelete = j.path("can_hard_delete").guess();
    this.canReact = j.path("can_react").guess();
    this.canSoftDelete = j.path("can_soft_delete").guess();
    this.canViewAttachments = j.path("can_view_attachments").guess();
    this.isFirstPost = j.path("is_first_post").guess();
    this.isLastPost = j.path("is_last_post").guess();
    this.lastEditDate = j.path("last_edit_date").guess();
    this.message = j.path("message").guess();
    this.messageState = j.path("message_state").guess();
    this.position = j.path("position").guess();
    this.postDate = j.path("post_date").guess();
    this.postId = j.path("post_id").guess();
    this.reactionScore = j.path("reaction_score").guess();
    this.threadId = j.path("thread_id").guess();
    this.warningMessage = j.path("warning_message").guess();
    this.isReactedTo = j.path("is_reacted_to").guess() ?? false;
    this.visitorReactionId = j.path("visitor_reaction_id").guess();

    if (this.isDamaged()) {
      errorIsDamaged.throwError(json: j);
    }

    this.user = User.fromWeb(j.path("User"));
    this.attachments = j.path("Attachments").map((j) {
      return Attachment.fromWeb(j);
    });
    try {
      this.thread = Thread.fromWeb(j.path("Thread"));
    } catch (e) { }
  }

  bool isDamaged() {
    return this.attachCount == null
        || this.canEdit == null
        || this.canHardDelete == null
        || this.canReact == null
        || this.canSoftDelete == null
        || this.canViewAttachments == null
        || this.isFirstPost == null
        || this.isLastPost == null
        || this.lastEditDate == null
        || this.message == null
        || this.messageState == null
        || this.position == null
        || this.postDate == null
        || this.postId == null
        || this.reactionScore == null
        || this.threadId == null
        || this.warningMessage == null;
  }

  @override
  bool operator ==(other) {
    return this.postId == other.postId;
  }
  @override
  int get hashCode => postId.hashCode;
}