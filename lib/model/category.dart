import 'package:d7forum_api/utils/utils.dart';

import 'node_type.dart';
import 'node.dart';

class Category extends Node {
  static bool predicate(Json j) {
    return j.path("node_type_id").value == NodeTypes.category;
  }

  Category({
    nodeId,
    parentNodeId,
    title,
    isVisible,
    description,
    nodeName,
  }) : super(
    nodeId: nodeId,
    parentNodeId: parentNodeId,
    title: title,
    displayInList: isVisible,
    description: description,
    nodeName: nodeName,
  );

  static final errorNodeTypeInvalid = D7ForumApiException("node_type_id != ${NodeTypes.category}");
  static final errorIsDamaged = D7ForumApiException("category.isDamaged() == true");
  Category.fromWeb(Json j) : super.fromJson(j) {
    if (!predicate(j)) {
      errorNodeTypeInvalid.throwError(json: j);
    }
    if (this.isDamaged()) {
      errorIsDamaged.throwError(json: j);
    }
  }
}