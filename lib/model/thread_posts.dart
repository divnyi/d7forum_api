import 'package:d7forum_api/utils/utils.dart';

import 'pagination.dart';
import 'post.dart';

class ThreadPosts {
  /// `posts`
  List<Post> posts;
  /// `pagination`
  Pagination pagination;

  ThreadPosts({
    this.posts,
    this.pagination
  });

  static final errorThreadsArrayIsNull = D7ForumApiException("json.posts is null");
  ThreadPosts.fromWeb(Json j) {
    final array = j.path("posts");
    if (array.isNull()) {
      errorThreadsArrayIsNull.throwError(json: j);
    }
    this.posts = array.map((j) {
      final item = Post.fromWeb(j);
      return item;
    });
    this.pagination = Pagination.fromWeb(j.path("pagination"));
  }
}