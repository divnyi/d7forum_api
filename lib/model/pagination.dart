import 'package:d7forum_api/utils/utils.dart';

class Pagination {
  /// `current_page`
  int currentPage;
  /// `last_page`
  int lastPage;
  /// `per_page`
  int perPage;
  /// `shown`
  int shown;
  /// `total`
  int total;

  Pagination({
    this.currentPage,
    this.lastPage,
    this.perPage,
    this.shown,
    this.total,
  });

  static final errorIsDamaged = D7ForumApiException("pagination.isDamaged() == true");
  Pagination.fromWeb(Json j) {
    this.currentPage = j.path("current_page").guess();
    this.lastPage = j.path("last_page").guess();
    this.perPage = j.path("per_page").guess();
    this.shown = j.path("shown").guess();
    this.total = j.path("total").guess();
    if (this.isDamaged()) {
      errorIsDamaged.throwError(json: j);
    }
  }

  bool isDamaged() {
    return this.currentPage == null
        || this.lastPage == null
        || this.perPage == null
        || this.shown == null
        || this.total == null;
  }
}