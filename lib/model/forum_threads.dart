import 'package:d7forum_api/utils/utils.dart';

import 'thread.dart';
import 'pagination.dart';

class ForumThreads {
  /// `threads`
  List<Thread> threads;
  /// `pagination`
  Pagination pagination;
  /// `sticky`
  List<Thread> sticky;

  ForumThreads({
    this.threads,
    this.pagination,
    this.sticky,
  });

  static final errorStickyArrayIsNull = D7ForumApiException("json.sticky is null");
  static final errorThreadsArrayIsNull = D7ForumApiException("json.threads is null");
  ForumThreads.fromWeb(Json j) {
    final sticky = j.path("sticky");
    if (sticky.isNull()) {
      errorThreadsArrayIsNull.throwError(json: j);
    }
    this.sticky = sticky.map((j) => Thread.fromWeb(j));
    
    final threads = j.path("threads");
    if (threads.isNull()) {
      errorThreadsArrayIsNull.throwError(json: j);
    }
    this.threads = threads.map((j) => Thread.fromWeb(j));

    this.pagination = Pagination.fromWeb(j.path("pagination"));
  }
}