import 'package:d7forum_api/utils/utils.dart';

import 'category.dart';
import 'forum.dart';

class Node {
  /// `node_id`
  int nodeId;
  /// `parent_node_id`
  int parentNodeId;
  /// `title`
  String title;
  /// `display_in_list`
  bool displayInList;
  /// `description`
  String description;
  /// `node_name`
  String nodeName;

  Node({
    this.nodeId,
    this.parentNodeId,
    this.title,
    this.displayInList,
    this.description,
    this.nodeName
  });

  Node.fromJson(Json j) {
    this.nodeId = j.path("node_id").guess();
    this.parentNodeId = j.path("parent_node_id").guess();
    this.title = j.path("title").guess();
    this.displayInList = j.path("display_in_list").guess();
    this.description = j.path("description").guess();
    this.nodeName = j.path("node_name").guess();
  }

  static final errorNodeTypeIsUnknown = D7ForumApiException("node_type is unknown");
  factory Node.fromWeb(Json j) {

    Node node = silentCatch(() => Category.fromWeb(j), [Category.errorNodeTypeInvalid])
             ?? silentCatch(() => Forum.fromWeb(j), [Forum.errorNodeTypeInvalid]);
    if (node == null) {
      errorNodeTypeIsUnknown.throwError(json: j);
    }
    return node;
  }

  bool isDamaged() {
    return nodeId == null
        || parentNodeId == null
        || title == null
        || displayInList == null
        || description == null;
  }

  @override
  bool operator ==(other) {
    return this.nodeId == other.nodeId;
  }
  @override
  int get hashCode => nodeId.hashCode;
}