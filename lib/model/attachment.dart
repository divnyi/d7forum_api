import 'package:d7forum_api/utils/utils.dart';

class Attachment {
  /// attach_date
  DateTime attachDate;
  /// attachment_id
  int attachmentId;
  /// content_id
  int contentId;
  /// content_type
  String contentType;
  /// file_size
  int fileSize;
  /// filename
  String filename;
  /// height
  int height;
  /// thumbnail_url
  String thumbnailUrl;
  /// video_url
  String videoUrl;
  /// view_count
  int viewCount;
  /// width
  int width;

  Attachment({
    this.attachDate,
    this.attachmentId,
    this.contentId,
    this.contentType,
    this.fileSize,
    this.filename,
    this.height,
    this.thumbnailUrl,
    this.videoUrl,
    this.viewCount,
    this.width,
  });

  static final errorIsDamaged = D7ForumApiException("post.isDamaged() == true");
  Attachment.fromWeb(Json j) {
    this.attachDate = j.path("attach_date").guess();
    this.attachmentId = j.path("attachment_id").guess();
    this.contentId = j.path("content_id").guess();
    this.contentType = j.path("content_type").guess();
    this.fileSize = j.path("file_size").guess();
    this.filename = j.path("filename").guess();
    this.height = j.path("height").guess();
    this.thumbnailUrl = j.path("thumbnail_url").guess();
    this.videoUrl = j.path("video_url").guess();
    this.viewCount = j.path("view_count").guess();
    this.width = j.path("width").guess();

    if (this.isDamaged()) {
      errorIsDamaged.throwError(json: j);
    }
  }

  bool isDamaged() {
    return this.attachDate == null
        || this.attachmentId == null
        || this.contentId == null
        || this.contentType == null
        || this.fileSize == null
        || this.filename == null
        || this.height == null
        || this.viewCount == null
        || this.width == null;
  }
}