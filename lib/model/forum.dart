import 'package:d7forum_api/utils/utils.dart';

import 'node_type.dart';
import 'node.dart';

class Forum extends Node {
  static bool predicate(Json j) {
    return j.path("node_type_id").value == NodeTypes.forum;
  }

  /// `type_data.discussion_count`
  int discussionCount;
  /// `type_data.message_count`
  int messageCount;

  /// `type_data.last_post_username`
  String lastPostUsername;
  /// `type_data.last_thread_title`
  String lastThreadTitle;
  /// `type_data.last_post_date`
  DateTime lastPostDate;

  /// `type_data.last_post_id`
  int lastPostId;
  /// `type_data.last_thread_id`
  int lastThreadId;

  /// `type_data.allow_poll`
  bool allowPoll;
  /// `type_data.allow_posting`
  bool allowPosting;
  /// `type_data.can_create_thread`
  bool canCreateThread;
  /// `type_data.can_upload_attachment`
  bool canUploadAttachment;

  Forum({
    this.discussionCount,
    this.messageCount,
    this.lastPostUsername,
    this.lastThreadTitle,
    this.lastPostDate,
    this.lastPostId,
    this.lastThreadId,
    this.allowPoll,
    this.allowPosting,
    this.canCreateThread,
    this.canUploadAttachment,

    nodeId,
    parentNodeId,
    title,
    isVisible,
    description,
    nodeName,
  }) : super(
    nodeId: nodeId,
    parentNodeId: parentNodeId,
    title: title,
    displayInList: isVisible,
    description: description,
    nodeName: nodeName,
  );

  static final errorNodeTypeInvalid = D7ForumApiException("node_type_id != ${NodeTypes.forum}");
  static final errorIsDamaged = D7ForumApiException("forum.isDamaged() == true");
  Forum.fromWeb(Json j) : super.fromJson(j) {
    if (!predicate(j)) {
      errorNodeTypeInvalid.throwError(json: j);
    }

    Json typeData = j.path("type_data");

    this.discussionCount = typeData.path("discussion_count").guess();
    this.messageCount = typeData.path("message_count").guess();

    this.lastPostUsername = typeData.path("last_post_username").guess();
    this.lastThreadTitle = typeData.path("last_thread_title").guess();
    this.lastPostDate = typeData.path("last_post_date").guess();

    this.lastPostId = typeData.path("last_post_id").guess();
    this.lastThreadId = typeData.path("last_thread_id").guess();

    this.allowPoll = typeData.path("allow_poll").guess();
    this.allowPosting = typeData.path("allow_posting").guess();
    this.canCreateThread = typeData.path("can_create_thread").guess();
    this.canUploadAttachment = typeData.path("can_upload_attachment").guess();

    if (this.isDamaged()) {
      errorIsDamaged.throwError(json: j);
    }
  }

  @override
  bool isDamaged() {
    return super.isDamaged()
        || this.discussionCount == null
        || this.messageCount == null
        || this.lastPostUsername == null
        || this.lastThreadTitle == null
        || this.lastPostDate == null
        || this.lastPostId == null
        || this.lastThreadId == null
        || this.allowPoll == null
        || this.allowPosting == null
        || this.canCreateThread == null
        || this.canUploadAttachment == null;
  }
}