import 'package:d7forum_api/utils/json.dart';
import 'package:d7forum_api/utils/d7forum_api_exception.dart';

class User {
  /// avatar_urls.o
  String avatarOriginalUrl;
  /// avatar_urls.h
  String avatarHighUrl;
  /// avatar_urls.l
  String avatarLargeUrl;
  /// avatar_urls.m
  String avatarMediumUrl;
  /// avatar_urls.s
  String avatarSmallUrl;

  /// can_ban
  bool canBan;
  /// can_converse
  bool canConverse;
  /// can_edit
  bool canEdit;
  /// can_follow
  bool canFollow;
  /// can_ignore
  bool canIgnore;
  /// can_post_profile
  bool canPostProfile;
  /// can_view_profile
  bool canViewProfile;
  /// can_view_profile_posts
  bool canViewProfilePosts;
  /// can_warn
  bool canWarn;
  /// is_staff
  bool isStaff;
  /// last_activity
  DateTime lastActivity;
  /// location
  String location;
  /// message_count
  int messageCount;
  /// reaction_score
  int reactionScore;
  /// register_date
  DateTime registerDate;
  /// signature
  String signature;
  /// trophy_points
  int trophyPoints;
  /// user_id
  int userId;
  /// user_title
  String userTitle;
  /// username
  String username;
  /// age
  int age;
  /// custom_fields
  Json customFields;
  /// custom_title
  String customTitle;
  /// is_followed
  bool isFollowed;
  /// is_ignored
  bool isIgnored;

  // TODO: add permission-specific fields:
  // https://xenforo.com/community/pages/api-endpoints/#type_User

  User({
    this.avatarOriginalUrl,
    this.avatarHighUrl,
    this.avatarLargeUrl,
    this.avatarMediumUrl,
    this.avatarSmallUrl,
    this.canBan,
    this.canConverse,
    this.canEdit,
    this.canFollow,
    this.canIgnore,
    this.canPostProfile,
    this.canViewProfile,
    this.canViewProfilePosts,
    this.canWarn,
    this.isStaff,
    this.lastActivity,
    this.location,
    this.messageCount,
    this.reactionScore,
    this.registerDate,
    this.signature,
    this.trophyPoints,
    this.userId,
    this.userTitle,
    this.username,
    this.age,
    this.customFields,
    this.customTitle,
    this.isFollowed,
    this.isIgnored,
  });

  static final errorIsDamaged = D7ForumApiException("user.isDamaged() == true");
  User.fromWeb(Json j) {
    final avatarUrls = j.path("avatar_urls");
    this.avatarOriginalUrl = avatarUrls.path("o").guess();
    this.avatarHighUrl = avatarUrls.path("h").guess();
    this.avatarLargeUrl = avatarUrls.path("l").guess();
    this.avatarMediumUrl = avatarUrls.path("m").guess();
    this.avatarSmallUrl = avatarUrls.path("s").guess();
    this.canBan = j.path("can_ban").guess();
    this.canConverse = j.path("can_converse").guess();
    this.canEdit = j.path("can_edit").guess();
    this.canFollow = j.path("can_follow").guess();
    this.canIgnore = j.path("can_ignore").guess();
    this.canPostProfile = j.path("can_post_profile").guess();
    this.canViewProfile = j.path("can_view_profile").guess();
    this.canViewProfilePosts = j.path("can_view_profile_posts").guess();
    this.canWarn = j.path("can_warn").guess();
    this.isStaff = j.path("is_staff").guess();
    this.lastActivity = j.path("last_activity").guess();
    this.location = j.path("location").guess();
    this.messageCount = j.path("message_count").guess();
    this.reactionScore = j.path("reaction_score").guess();
    this.registerDate = j.path("register_date").guess();
    this.signature = j.path("signature").guess();
    this.trophyPoints = j.path("trophy_points").guess();
    this.userId = j.path("user_id").guess();
    this.userTitle = j.path("user_title").guess();
    this.username = j.path("username").guess();
    this.age = j.path("age").guess();
    this.customFields = j.path("custom_fields").guess();
    this.customTitle = j.path("custom_title").guess();
    this.isFollowed = j.path("is_followed").guess();
    this.isIgnored = j.path("is_ignored").guess();

    if (this.isDamaged()) {
      errorIsDamaged.throwError(json: j);
    }
  }

  bool isDamaged() {
    return this.canBan == null
        || this.canConverse == null
        || this.canEdit == null
        || this.canFollow == null
        || this.canIgnore == null
        || this.canPostProfile == null
        || this.canViewProfile == null
        || this.canViewProfilePosts == null
        || this.canWarn == null
        || this.isStaff == null
        || this.lastActivity == null
        || this.location == null
        || this.messageCount == null
        || this.reactionScore == null
        || this.registerDate == null
        || this.signature == null
        || this.trophyPoints == null
        || this.userId == null
        || this.userTitle == null
        || this.username == null;
  }
}