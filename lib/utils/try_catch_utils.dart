T silentCatch<T>(T f(), List<Exception> exceptions) {
  try {
    return f();
  } catch (e) {
    if (exceptions.firstWhere((e2) {
          return e == e2;
        }, orElse: () => null) == null) {
      rethrow;
    }
    return null;
  }
}