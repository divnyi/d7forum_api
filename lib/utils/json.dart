import 'dart:convert' as JSON;

class Json {
  dynamic value;
  Json(this.value);
  factory Json.fromString(String str) {
    var json;
    try {
      json = JSON.jsonDecode(str);
    } catch (e) {}
    return Json(json);
  }

  Json idx(int idx) {
    if (value == null) return Json(null);
    return Json(value[idx]);
  }

  Json path(String str) {
    if (value == null) return Json(null);
    return Json(value[str]);
  }

  bool contains(dynamic obj) {
    if (value == null) return false;
    return this.value.contains(obj);
  }

  bool isNull() {
    return this.value == null;
  }

  List<T> map<T>(T f(Json j)) {
    return toArray().map(f).toList();
  }

  List<Json> toArray() {
    if (value is List) {
      return (value as List).map((arg) {
        return Json(arg);
      }).toList();
    }
    return List<Json>();
  }

  DateTime toDate() {
    if (value is int && value != 0) {
      return DateTime.fromMillisecondsSinceEpoch(
        value * 1000,
      );
    }
    return null;
  }

  int toInt() {
    if (value is int) {
      return value;
    }
    return null;
  }

  String toStr() {
    if (value is String) {
      return value;
    }
    return null;
  }

  bool toBool() {
    if (value is bool) {
      return value;
    }
    return null;
  }

  T guess<T>() {
    switch (T) {
      case int:
        return toInt() as T;
      case String:
        return toStr() as T;
      case bool:
        return toBool() as T;
      case DateTime:
        return toDate() as T;
      case Json:
        return this as T;
      default:
        return null;
    }
  }

  @override
  String toString() {
    return "<Json: $value>";
  }
}