import 'package:d7forum_api/utils/json.dart';

class D7ForumApiException implements Exception {
  String cause;
  Json json;
  int httpCode;
  D7ForumApiException(this.cause);

  @override
  bool operator ==(other) {
    return this.cause == other.cause;
  }

  @override
  int get hashCode => cause.hashCode;

  @override
  String toString() {
    return "<D7ForumApiException: $cause; httpCode: $httpCode, json: $json>";
  }

  void throwError({Json json, int httpCode}) {
    this.json = json;
    this.httpCode = httpCode;
    throw this;
  }
}