import 'package:d7forum_api/utils/json.dart';
import 'package:d7forum_api/utils/d7forum_api_exception.dart';
import 'package:http/http.dart' as http;

class BaseNetwork {
  // global variables
  /// api key provided by forum.sokyra.party owners
  static String apiKey;
  /// url of the forum
  static String serverUrl = "forum.sokyra.party";
  /// url of the forum
  static String baseUrl = "api/";

  // exceptions
  static final errorApiKeyMissing = D7ForumApiException("XF-Api-Key is missing. You must initialize BaseNetwork.apiKey with your own key, taken from d7 admins");
  static final errorNetworkException = D7ForumApiException("network issue");
  static final errorJsonInvalid = D7ForumApiException("json invalid");

  // abstract class part
  static Future<Json> get(String route, { Map<String, String> queryParams }) async {
    if (apiKey == null) {
      throw errorApiKeyMissing;
    }
    final response = await http.get(
      Uri.https(serverUrl, baseUrl+route, queryParams),
      headers: {
        "XF-Api-Key" : apiKey,
        "Content-Type" : "application/x-www-form-urlencoded",
      }
    );
    final j = Json.fromString(response.body);
    if (response.statusCode != 200) {
      errorNetworkException.throwError(
        httpCode: response.statusCode,
        json: j,
      );
    }
    if (j.value == null && response.body != "") {
      errorJsonInvalid.throwError(
        httpCode: response.statusCode,
        json: j,
      );
    }
    return j;
  }
}