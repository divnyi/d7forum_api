import 'nodes_api.dart';
import 'forum_threads_api.dart';
import 'thread_posts_api.dart';

class API {
  static final API shared = API();

  // every API is variable so you can mock
  NodesAPI nodes;
  ForumThreadsAPI forumThreads;
  ThreadPostsAPI threadPosts;

  API() {
    setup();
  }

  void setup() {
    nodes = NodesAPIimpl();
    forumThreads = ForumThreadsAPIimpl();
    threadPosts = ThreadPostsAPIimpl();
  }
}