import 'base_network.dart';

import 'package:d7forum_api/model/forum_threads.dart';

abstract class ForumThreadsAPI {
  Future<ForumThreads> fetch(int forumId, { int page });
}

class ForumThreadsAPIimpl implements ForumThreadsAPI {
  Future<ForumThreads> fetch(int forumId, { int page = 1 }) async {
    final j = await BaseNetwork.get("forums/$forumId/threads", queryParams: {
      "page" : "$page",
    });
    final forumThreads = ForumThreads.fromWeb(j);
    return forumThreads;
  }
}