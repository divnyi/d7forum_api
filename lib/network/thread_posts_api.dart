import 'base_network.dart';

import 'package:d7forum_api/model/thread_posts.dart';

abstract class ThreadPostsAPI {
  Future<ThreadPosts> fetch(int threadId, { int page });
}

class ThreadPostsAPIimpl implements ThreadPostsAPI {
  Future<ThreadPosts> fetch(int threadId, { int page = 1 }) async {
    final j = await BaseNetwork.get("threads/$threadId/posts", queryParams: {
      "page" : "$page",
    });
    final forumThreads = ThreadPosts.fromWeb(j);
    return forumThreads;
  }
}