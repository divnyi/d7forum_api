import 'package:d7forum_api/utils/d7forum_api_exception.dart';

import 'base_network.dart';

import 'package:d7forum_api/model/node.dart';

abstract class NodesAPI {
  Future<List<Node>> fetch();
}

class NodesAPIimpl implements NodesAPI {
  static final errorNodesArrayIsNull = D7ForumApiException("json.nodes_flat is null");
  Future<List<Node>> fetch() async {
    final j = await BaseNetwork.get("nodes/flattened");
    final nodesFlat = j.path("nodes_flat");
    if(nodesFlat.isNull()) {
      errorNodesArrayIsNull.throwError(json: j);
    }
    final nodes = nodesFlat.map((item) {
      final j = item.path("node");
      final node = Node.fromWeb(j);
      return node;
    });
    return nodes;
  }
}