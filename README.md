# d7forum_api

Network layer and models for forum.sokyra.party

## Installation

Add to `pubspec.yaml`:

```
dependencies:
  d7forum_api:
    git: git@gitlab.com:divnyi/d7forum_api.git
```

## API references

https://xenforo.com/xf2-docs/dev/rest-api/

https://xenforo.com/community/pages/api-endpoints/

