import 'package:flutter_test/flutter_test.dart';

import 'package:d7forum_api/d7forum_api.dart';

// this whole file is just one line:
// final apiKey = "<your api key>";
import 'secret.dart' as secret;

class MockNodesAPI implements NodesAPI {
  Future<List<Node>> fetch() async {
    return [Node(title: "mocked!")];
  }
}

void main() {
  BaseNetwork.apiKey = secret.apiKey;
  API.shared.setup();
  test('fetch', () async {
    final nodes = await API.shared.nodes.fetch();
    expect(nodes.length > 0, true);
  });
  test('mocked', () async {
    API.shared.nodes = MockNodesAPI();
    final nodes = await API.shared.nodes.fetch();
    expect(nodes.first.title, "mocked!");
  });
}
