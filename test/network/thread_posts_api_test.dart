import 'package:flutter_test/flutter_test.dart';

import 'package:d7forum_api/d7forum_api.dart';

// this whole file is just one line:
// final apiKey = "<your api key>";
import 'secret.dart' as secret;

class MockThreadAPI implements ThreadPostsAPI {
  Future<ThreadPosts> fetch(int forumId, { int page }) async {
    return ThreadPosts(
        posts: [Post(message: "mocked!")]
    );
  }
}

void main() {
  BaseNetwork.apiKey = secret.apiKey;
  API.shared.setup();
  test('fetch', () async {
    final forumThreads = await API.shared.threadPosts.fetch(90);
    expect(forumThreads.posts.length > 0, true);
  });
  test('fetch 9999 page', () async {
    try {
      await API.shared.threadPosts.fetch(90, page: 9999);
      fail("expecting throw");
    } on D7ForumApiException catch (e) {
      expect(e, BaseNetwork.errorNetworkException);
      expect(e.json.path("errors").toArray().first?.path("code")?.toStr(), "invalid_page");
    }
  });
  test('mocked', () async {
    API.shared.threadPosts = MockThreadAPI();
    final forumThreads = await API.shared.threadPosts.fetch(90);
    expect(forumThreads.posts.first.message, "mocked!");
  });
}
