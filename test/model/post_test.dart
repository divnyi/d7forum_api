import 'package:flutter_test/flutter_test.dart';

import 'package:d7forum_api/d7forum_api.dart';

import '../test_utils.dart';

void main() {
  final successString = """
  {
            "attach_count": 1,
            "Attachments": [
                {
                    "attach_date": 1556006361,
                    "attachment_id": 57,
                    "content_id": 862,
                    "content_type": "post",
                    "file_size": 16335,
                    "filename": "Screenshot_2019-04-23-Киев-Оперативный---Публикации.jpg",
                    "height": 262,
                    "thumbnail_url": "https://forum.sokyra.party/data/attachments/0/57-f8ef87978f14c29ae247b678e7c64719.jpg",
                    "view_count": 23,
                    "width": 522
                }
            ],
            "can_edit": false,
            "can_hard_delete": false,
            "can_react": false,
            "can_soft_delete": false,
            "can_view_attachments": false,
            "is_first_post": false,
            "is_last_post": false,
            "last_edit_date": 0,
            "message": "some message~",
            "message_state": "visible",
            "position": 4,
            "post_date": 1556006415,
            "post_id": 862,
            "reaction_score": 0,
            "thread_id": 90,
            "User": {
                "avatar_urls": {
                    "o": "https://secure.gravatar.com/avatar/6bfc618ef213d1d244e9b89772fb6a90?s=384",
                    "h": "https://secure.gravatar.com/avatar/6bfc618ef213d1d244e9b89772fb6a90?s=384",
                    "l": "https://secure.gravatar.com/avatar/6bfc618ef213d1d244e9b89772fb6a90?s=192",
                    "m": "https://secure.gravatar.com/avatar/6bfc618ef213d1d244e9b89772fb6a90?s=96",
                    "s": "https://secure.gravatar.com/avatar/6bfc618ef213d1d244e9b89772fb6a90?s=48"
                },
                "can_ban": false,
                "can_converse": false,
                "can_edit": false,
                "can_follow": false,
                "can_ignore": false,
                "can_post_profile": false,
                "can_view_profile": false,
                "can_view_profile_posts": false,
                "can_warn": false,
                "is_staff": false,
                "last_activity": 1556281163,
                "location": "Kyiv, 04071",
                "message_count": 5,
                "reaction_score": 0,
                "register_date": 1543837109,
                "signature": "",
                "trophy_points": 0,
                "user_id": 33,
                "user_title": "Активіст",
                "username": "wavezz"
            },
            "user_id": 33,
            "username": "wavezz",
            "warning_message": ""
        }
  """;
  test('from JSON', () {
    final jsonString = successString;
    final json = Json.fromString(jsonString);
    final item = Post.fromWeb(json);
    expect(item.isDamaged(), false);

    expect(item.attachCount, 1);
    expect(item.canEdit, false);
    expect(item.canHardDelete, false);
    expect(item.canReact, false);
    expect(item.canSoftDelete, false);
    expect(item.canViewAttachments, false);
    expect(item.isFirstPost, false);
    expect(item.isLastPost, false);
    expect(item.lastEditDate, 0);
    expect(item.message, "some message~");
    expect(item.messageState, "visible");
    expect(item.position, 4);
    expect(item.postDate, TestUtils.formDate(1556006415));
    expect(item.postId, 862);
    expect(item.reactionScore, 0);
    expect(item.threadId, 90);
    expect(item.warningMessage, "");
  });

  test('equality test', () {
    final jsonString = successString;
    final json = Json.fromString(jsonString);
    final item = Post.fromWeb(json);
    final item2 = Post(postId: item.postId);
    expect(item, item2);
  });

  test('from bad json', () {
    final jsonString = """
  {
                "breadc
  """;
    final json = Json.fromString(jsonString);
    expect(() => Post.fromWeb(json), throwsA(Post.errorIsDamaged));
  });

  test('field missing', () {
    final jsonString = """
  {
            "attach_count": 1,
            "Attachments": [
                {
                    "attach_date": 1556006361,
                    "attachment_id": 57,
                    "content_id": 862,
                    "content_type": "post",
                    "file_size": 16335,
                    "filename": "Screenshot_2019-04-23-Киев-Оперативный---Публикации.jpg",
                    "height": 262,
                    "thumbnail_url": "https://forum.sokyra.party/data/attachments/0/57-f8ef87978f14c29ae247b678e7c64719.jpg",
                    "view_count": 23,
                    "width": 522
                }
            ],
            "can_edit": false,
            "can_hard_delete": false,
            "can_soft_delete": false,
            "can_view_attachments": false,
            "is_first_post": false,
            "is_last_post": false,
            "last_edit_date": 0,
            "message": "some message~",
            "message_state": "visible",
            "position": 4,
            "post_date": 1556006415,
            "post_id": 862,
            "reaction_score": 0,
            "thread_id": 90,
            "User": {
                "avatar_urls": {
                    "o": "https://secure.gravatar.com/avatar/6bfc618ef213d1d244e9b89772fb6a90?s=384",
                    "h": "https://secure.gravatar.com/avatar/6bfc618ef213d1d244e9b89772fb6a90?s=384",
                    "l": "https://secure.gravatar.com/avatar/6bfc618ef213d1d244e9b89772fb6a90?s=192",
                    "m": "https://secure.gravatar.com/avatar/6bfc618ef213d1d244e9b89772fb6a90?s=96",
                    "s": "https://secure.gravatar.com/avatar/6bfc618ef213d1d244e9b89772fb6a90?s=48"
                },
                "can_ban": false,
                "can_converse": false,
                "can_edit": false,
                "can_follow": false,
                "can_ignore": false,
                "can_post_profile": false,
                "can_view_profile": false,
                "can_view_profile_posts": false,
                "can_warn": false,
                "is_staff": false,
                "last_activity": 1556281163,
                "location": "Kyiv, 04071",
                "message_count": 5,
                "reaction_score": 0,
                "register_date": 1543837109,
                "signature": "",
                "trophy_points": 0,
                "user_id": 33,
                "user_title": "Активіст",
                "username": "wavezz"
            },
            "user_id": 33,
            "username": "wavezz",
            "warning_message": ""
        }
  """;
    final json = Json.fromString(jsonString);
    expect(() => Post.fromWeb(json), throwsA(Post.errorIsDamaged));
  });
}
