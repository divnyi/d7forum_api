import 'package:flutter_test/flutter_test.dart';

import 'package:d7forum_api/d7forum_api.dart';

import '../test_utils.dart';

void main() {
  test('from JSON', () {
    final jsonString = """
  {
                    "attach_date": 1556005820,
                    "attachment_id": 56,
                    "content_id": 861,
                    "content_type": "post",
                    "file_size": 67485,
                    "filename": "Screenshot_2019-04-23 Facebook.png",
                    "height": 815,
                    "thumbnail_url": "https://forum.sokyra.party/data/attachments/0/56-7ef4177982d00b979a294dcddb57c5ef.jpg",
                    "view_count": 24,
                    "width": 538
                }
  """;
    final json = Json.fromString(jsonString);
    final item = Attachment.fromWeb(json);
    expect(item.isDamaged(), false);

    expect(item.attachDate, TestUtils.formDate(1556005820));
    expect(item.attachmentId, 56);
    expect(item.contentId, 861);
    expect(item.contentType, "post");
    expect(item.fileSize, 67485);
    expect(item.filename, "Screenshot_2019-04-23 Facebook.png");
    expect(item.height, 815);
    expect(item.thumbnailUrl, "https://forum.sokyra.party/data/attachments/0/56-7ef4177982d00b979a294dcddb57c5ef.jpg");
    expect(item.viewCount, 24);
    expect(item.width, 538);
  });

  test('from bad json', () {
    final jsonString = """
  {
                "breadc
  """;
    final json = Json.fromString(jsonString);
    expect(() => Attachment.fromWeb(json), throwsA(Attachment.errorIsDamaged));
  });

  test('field missing', () {
    final jsonString = """
  {
                    "attach_date": 1556005820,
                    "attachment_id": 56,
                    "content_id": 861,
                    "content_type": "post",
                    "filename": "Screenshot_2019-04-23 Facebook.png",
                    "height": 815,
                    "thumbnail_url": "https://forum.sokyra.party/data/attachments/0/56-7ef4177982d00b979a294dcddb57c5ef.jpg",
                    "view_count": 24,
                    "width": 538
                }
  """;
    final json = Json.fromString(jsonString);
    expect(() => Attachment.fromWeb(json), throwsA(Attachment.errorIsDamaged));
  });
}
