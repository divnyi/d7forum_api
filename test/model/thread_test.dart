import 'package:flutter_test/flutter_test.dart';

import 'package:d7forum_api/d7forum_api.dart';

import '../test_utils.dart';

void main() {
  final successString = """
 {
            "can_edit": false,
            "can_edit_tags": false,
            "can_hard_delete": false,
            "can_reply": false,
            "can_soft_delete": false,
            "can_view_attachments": false,
            "custom_fields": {},
            "discussion_open": true,
            "discussion_state": "visible",
            "discussion_type": "",
            "first_post_id": 872,
            "first_post_reaction_score": 0,
            "last_post_date": 1556055318,
            "last_post_id": 878,
            "last_post_user_id": 1,
            "last_post_username": "Помазан Богдан",
            "node_id": 11,
            "post_date": 1556026448,
            "prefix_id": 0,
            "reply_count": 1,
            "sticky": false,
            "tags": [],
            "thread_id": 93,
            "title": "Домашня сторінка сайту",
            "User": {
                "avatar_urls": {
                    "o": "https://secure.gravatar.com/avatar/66330b3ac39491a82fea8985c5048877?s=384",
                    "h": "https://secure.gravatar.com/avatar/66330b3ac39491a82fea8985c5048877?s=384",
                    "l": "https://secure.gravatar.com/avatar/66330b3ac39491a82fea8985c5048877?s=192",
                    "m": "https://secure.gravatar.com/avatar/66330b3ac39491a82fea8985c5048877?s=96",
                    "s": "https://secure.gravatar.com/avatar/66330b3ac39491a82fea8985c5048877?s=48"
                },
                "can_ban": false,
                "can_converse": false,
                "can_edit": false,
                "can_follow": false,
                "can_ignore": false,
                "can_post_profile": false,
                "can_view_profile": false,
                "can_view_profile_posts": false,
                "can_warn": false,
                "is_staff": false,
                "last_activity": 1556087820,
                "location": "Київ",
                "message_count": 3,
                "reaction_score": 1,
                "register_date": 1543845316,
                "signature": "",
                "trophy_points": 0,
                "user_id": 119,
                "user_title": "Активіст",
                "username": "Serp"
            },
            "user_id": 119,
            "username": "Serp",
            "view_count": 41
        }
  """;
  test('from JSON', () {
    final jsonString = successString;
    final json = Json.fromString(jsonString);
    final item = Thread.fromWeb(json);
    expect(item.isDamaged(), false);

    expect(item.canEdit, false);
    expect(item.canEditTags, false);
    expect(item.canHardDelete, false);
    expect(item.canReply, false);
    expect(item.canSoftDelete, false);
    expect(item.canViewAttachments, false);
    expect(item.discussionOpen, true);
    expect(item.discussionState, "visible");
    expect(item.discussionType, "");
    expect(item.firstPostId, 872);
    expect(item.firstPostReactionScore, 0);
    expect(item.lastPostDate, TestUtils.formDate(1556055318));
    expect(item.lastPostId, 878);
    expect(item.lastPostUserId, 1);
    expect(item.lastPostUsername, "Помазан Богдан");
    expect(item.nodeId, 11);
    expect(item.postDate, TestUtils.formDate(1556026448));
    expect(item.prefixId, 0);
    expect(item.replyCount, 1);
    expect(item.sticky, false);
    expect(item.threadId, 93);
    expect(item.title, "Домашня сторінка сайту");
    expect(item.viewCount, 41);
  });

  test('equality test', () {
    final jsonString = successString;
    final json = Json.fromString(jsonString);
    final item = Thread.fromWeb(json);
    final item2 = Thread(threadId: item.threadId);
    expect(item, item2);
  });

  test('from bad json', () {
    final jsonString = """
  {
                "breadc
  """;
    final json = Json.fromString(jsonString);
    expect(() => Thread.fromWeb(json), throwsA(Thread.errorIsDamaged));
  });

  test('field missing', () {
    final jsonString = """
   {
            "can_edit": false,
            "can_edit_tags": false,
            "can_hard_delete": false,
            "can_reply": false,
            "can_soft_delete": false,
            "can_view_attachments": false,
            "custom_fields": {},
            "discussion_open": true,
            "discussion_state": "visible",
            "discussion_type": "",
            "first_post_id": 872,
            "first_post_reaction_score": 0,
            "last_post_date": 1556055318,
            "last_post_id": 878,
            "last_post_user_id": 1,
            "last_post_username": "Помазан Богдан",
            "node_id": 11,
            "post_date": 1556026448,
            "prefix_id": 0,
            "reply_count": 1,
            "sticky": false,
            "title": "Домашня сторінка сайту",
            "User": {
                "avatar_urls": {
                    "o": "https://secure.gravatar.com/avatar/66330b3ac39491a82fea8985c5048877?s=384",
                    "h": "https://secure.gravatar.com/avatar/66330b3ac39491a82fea8985c5048877?s=384",
                    "l": "https://secure.gravatar.com/avatar/66330b3ac39491a82fea8985c5048877?s=192",
                    "m": "https://secure.gravatar.com/avatar/66330b3ac39491a82fea8985c5048877?s=96",
                    "s": "https://secure.gravatar.com/avatar/66330b3ac39491a82fea8985c5048877?s=48"
                },
                "can_ban": false,
                "can_converse": false,
                "can_edit": false,
                "can_follow": false,
                "can_ignore": false,
                "can_post_profile": false,
                "can_view_profile": false,
                "can_view_profile_posts": false,
                "can_warn": false,
                "is_staff": false,
                "last_activity": 1556087820,
                "location": "Київ",
                "message_count": 3,
                "reaction_score": 1,
                "register_date": 1543845316,
                "signature": "",
                "trophy_points": 0,
                "user_id": 119,
                "user_title": "Активіст",
                "username": "Serp"
            },
            "user_id": 119,
            "username": "Serp",
            "view_count": 41
        }
  """;
    final json = Json.fromString(jsonString);
    expect(() => Thread.fromWeb(json), throwsA(Thread.errorIsDamaged));
  });
}
