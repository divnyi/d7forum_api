import 'package:flutter_test/flutter_test.dart';

import 'package:d7forum_api/d7forum_api.dart';

void main() {
  final successString = """
  {
                "breadcrumbs": [],
                "description": "",
                "display_in_list": true,
                "display_order": 100,
                "node_id": 3,
                "node_name": null,
                "node_type_id": "Category",
                "parent_node_id": 0,
                "title": "Партійний офіс",
                "type_data": {}
            }
  """;

  test('from JSON', () {
    final jsonString = successString;
    final json = Json.fromString(jsonString);
    final item = Category.fromWeb(json);
    expect(item.isDamaged(), false);

    expect(item.displayInList, true);
    expect(item.nodeId, 3);
    expect(item.parentNodeId, 0);
    expect(item.description, "");
    expect(item.title, "Партійний офіс");
  });

  test('equality test', () {
    final jsonString = successString;
    final json = Json.fromString(jsonString);
    final item = Category.fromWeb(json);
    final item2 = Category(nodeId: item.nodeId);
    expect(item, item2);
  });

  test('from wrong node_type_id', () {
    final jsonString = """
  {
                "breadcrumbs": [],
                "description": "",
                "display_in_list": true,
                "display_order": 100,
                "node_id": 3,
                "node_name": null,
                "node_type_id": "Forum",
                "parent_node_id": 0,
                "title": "Партійний офіс",
                "type_data": {}
            }
  """;
    final json = Json.fromString(jsonString);
    expect(() => Category.fromWeb(json), throwsA(Category.errorNodeTypeInvalid));
  });

  test('from bad json', () {
    final jsonString = """
  {
                "breadc
  """;
    final json = Json.fromString(jsonString);
    expect(() => Category.fromWeb(json), throwsA(Category.errorNodeTypeInvalid));
  });

  test('node_id is suddenly string', () {
    final jsonString = """
  {
                "breadcrumbs": [],
                "description": "",
                "display_in_list": true,
                "display_order": 100,
                "node_id": "asd",
                "node_name": null,
                "node_type_id": "Category",
                "parent_node_id": 0,
                "title": "Партійний офіс",
                "type_data": {}
            }
  """;
    final json = Json.fromString(jsonString);
    expect(() => Category.fromWeb(json), throwsA(Category.errorIsDamaged));
  });
}
