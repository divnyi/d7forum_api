import 'package:flutter_test/flutter_test.dart';

import 'package:d7forum_api/d7forum_api.dart';

import '../test_utils.dart';

void main() {
  test('from JSON', () {
    final jsonString = """
 {
                "avatar_urls": {
                    "o": "https://forum.sokyra.party/data/avatars/o/0/1.jpg?1543577345",
                    "h": "https://forum.sokyra.party/data/avatars/h/0/1.jpg?1543577345",
                    "l": "https://forum.sokyra.party/data/avatars/l/0/1.jpg?1543577345",
                    "m": "https://forum.sokyra.party/data/avatars/m/0/1.jpg?1543577345",
                    "s": "https://forum.sokyra.party/data/avatars/s/0/1.jpg?1543577345"
                },
                "can_ban": false,
                "can_converse": false,
                "can_edit": false,
                "can_follow": false,
                "can_ignore": false,
                "can_post_profile": false,
                "can_view_profile": false,
                "can_view_profile_posts": false,
                "can_warn": false,
                "is_staff": true,
                "last_activity": 1556378999,
                "location": "",
                "message_count": 148,
                "reaction_score": 79,
                "register_date": 1543576917,
                "signature": "Міністр Центральних Процесорів",
                "trophy_points": 3,
                "user_id": 1,
                "user_title": "Міністр",
                "username": "Помазан Богдан"
            }
  """;
    final json = Json.fromString(jsonString);
    final item = User.fromWeb(json);
    expect(item.isDamaged(), false);

    expect(item.avatarOriginalUrl, "https://forum.sokyra.party/data/avatars/o/0/1.jpg?1543577345");
    expect(item.avatarHighUrl, "https://forum.sokyra.party/data/avatars/h/0/1.jpg?1543577345");
    expect(item.avatarLargeUrl, "https://forum.sokyra.party/data/avatars/l/0/1.jpg?1543577345");
    expect(item.avatarMediumUrl, "https://forum.sokyra.party/data/avatars/m/0/1.jpg?1543577345");
    expect(item.avatarSmallUrl, "https://forum.sokyra.party/data/avatars/s/0/1.jpg?1543577345");
    expect(item.canBan, false);
    expect(item.canConverse, false);
    expect(item.canEdit, false);
    expect(item.canFollow, false);
    expect(item.canIgnore, false);
    expect(item.canPostProfile, false);
    expect(item.canViewProfile, false);
    expect(item.canViewProfilePosts, false);
    expect(item.canWarn, false);
    expect(item.isStaff, true);
    expect(item.lastActivity, TestUtils.formDate(1556378999));
    expect(item.location, "");
    expect(item.messageCount, 148);
    expect(item.reactionScore, 79);
    expect(item.registerDate, TestUtils.formDate(1543576917));
    expect(item.signature, "Міністр Центральних Процесорів");
    expect(item.trophyPoints, 3);
    expect(item.userId, 1);
    expect(item.userTitle, "Міністр");
    expect(item.username, "Помазан Богдан");
  });

  test('from bad json', () {
    final jsonString = """
  {
                "breadc
  """;
    final json = Json.fromString(jsonString);
    expect(() => User.fromWeb(json), throwsA(User.errorIsDamaged));
  });

  test('field missing', () {
    final jsonString = """
  {
                "avatar_urls": {
                    "o": "https://forum.sokyra.party/data/avatars/o/0/1.jpg?1543577345",
                    "h": "https://forum.sokyra.party/data/avatars/h/0/1.jpg?1543577345",
                    "l": "https://forum.sokyra.party/data/avatars/l/0/1.jpg?1543577345",
                    "m": "https://forum.sokyra.party/data/avatars/m/0/1.jpg?1543577345",
                    "s": "https://forum.sokyra.party/data/avatars/s/0/1.jpg?1543577345"
                },
                "can_ban": false,
                "can_converse": false,
                "can_edit": false,
                "can_follow": false,
                "can_ignore": false,
                "can_post_profile": false,
                "can_view_profile": false,
                "can_view_profile_posts": false,
                "can_warn": false,
                "is_staff": true,
                "last_activity": 1556378999,
                "location": "",
                "message_count": 148,
                "reaction_score": 79,
                "register_date": 1543576917,
                "signature": "Міністр Центральних Процесорів",
                "user_title": "Міністр",
                "username": "Помазан Богдан"
            }
  """;
    final json = Json.fromString(jsonString);
    expect(() => User.fromWeb(json), throwsA(User.errorIsDamaged));
  });
}
