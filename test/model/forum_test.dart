import 'package:flutter_test/flutter_test.dart';

import 'package:d7forum_api/d7forum_api.dart';

import '../test_utils.dart';

void main() {
  final successString = """
            {
                "breadcrumbs": [
                    {
                        "node_id": 3,
                        "title": "Партійний офіс",
                        "node_type_id": "Category"
                    },
                    {
                        "node_id": 7,
                        "title": "Пресс-служба Демократичной Сокири",
                        "node_type_id": "Forum"
                    }
                ],
                "description": "Обговорення YouTube каналу партии",
                "display_in_list": true,
                "display_order": 100,
                "node_id": 26,
                "node_name": null,
                "node_type_id": "Forum",
                "parent_node_id": 7,
                "title": "Сокира TV",
                "type_data": {
                    "allow_poll": true,
                    "allow_posting": true,
                    "can_create_thread": false,
                    "can_upload_attachment": false,
                    "discussion_count": 1,
                    "last_post_date": 1544555291,
                    "last_post_id": 239,
                    "last_post_username": "Павло Громов",
                    "last_thread_id": 41,
                    "last_thread_prefix_id": 0,
                    "last_thread_title": "Ваши предложения по названию партийного ютуб канала",
                    "message_count": 8,
                    "min_tags": 0,
                    "require_prefix": false
                }
            }
    """;
  test('from JSON', () {
    final jsonString = successString;
    final json = Json.fromString(jsonString);
    final item = Forum.fromWeb(json);
    expect(item.isDamaged(), false);

    expect(item.displayInList, true);
    expect(item.nodeId, 26);
    expect(item.parentNodeId, 7);
    expect(item.title, "Сокира TV");
    expect(item.description, "Обговорення YouTube каналу партии");

    expect(item.allowPoll, true);
    expect(item.allowPosting, true);
    expect(item.canCreateThread, false);
    expect(item.canUploadAttachment, false);
    expect(item.discussionCount, 1);
    expect(item.lastPostDate, TestUtils.formDate(1544555291));
    expect(item.lastPostId, 239);
    expect(item.lastPostUsername, "Павло Громов");
    expect(item.lastThreadId, 41);
    expect(item.lastThreadTitle, "Ваши предложения по названию партийного ютуб канала");
    expect(item.messageCount, 8);
  });

  test('equality test', () {
    final jsonString = successString;
    final json = Json.fromString(jsonString);
    final item = Forum.fromWeb(json);
    final item2 = Forum(nodeId: item.nodeId);
    expect(item, item2);
  });

  test('from wrong node_type_id', () {
    final jsonString = """
            {
                "breadcrumbs": [
                    {
                        "node_id": 3,
                        "title": "Партійний офіс",
                        "node_type_id": "Category"
                    },
                    {
                        "node_id": 7,
                        "title": "Пресс-служба Демократичной Сокири",
                        "node_type_id": "Forum"
                    }
                ],
                "description": "Обговорення YouTube каналу партии",
                "display_in_list": true,
                "display_order": 100,
                "node_id": 26,
                "node_name": null,
                "node_type_id": "Category",
                "parent_node_id": 7,
                "title": "Сокира TV",
                "type_data": {
                    "allow_poll": true,
                    "allow_posting": true,
                    "can_create_thread": false,
                    "can_upload_attachment": false,
                    "discussion_count": 1,
                    "last_post_date": 1544555291,
                    "last_post_id": 239,
                    "last_post_username": "Павло Громов",
                    "last_thread_id": 41,
                    "last_thread_prefix_id": 0,
                    "last_thread_title": "Ваши предложения по названию партийного ютуб канала",
                    "message_count": 8,
                    "min_tags": 0,
                    "require_prefix": false
                }
            }
    """;
    final json = Json.fromString(jsonString);
    expect(() => Forum.fromWeb(json), throwsA(Forum.errorNodeTypeInvalid));
  });

  test('from bad json', () {
    final jsonString = """
  {
                "breadc
  """;
    final json = Json.fromString(jsonString);
    expect(() => Forum.fromWeb(json), throwsA(Forum.errorNodeTypeInvalid));
  });

  test('some field is missing', () {
    final jsonString = """
            {
                "breadcrumbs": [
                    {
                        "node_id": 3,
                        "title": "Партійний офіс",
                        "node_type_id": "Category"
                    },
                    {
                        "node_id": 7,
                        "title": "Пресс-служба Демократичной Сокири",
                        "node_type_id": "Forum"
                    }
                ],
                "description": "Обговорення YouTube каналу партии",
                "display_in_list": true,
                "node_name": null,
                "node_type_id": "Forum",
                "parent_node_id": 7,
                "title": "Сокира TV",
                "type_data": {
                    "allow_poll": true,
                    "allow_posting": true,
                    "can_create_thread": false,
                    "can_upload_attachment": false,
                    "discussion_count": 1,
                    "last_post_date": 1544555291,
                    "last_post_id": 239,
                    "last_post_username": "Павло Громов",
                    "last_thread_id": 41,
                    "last_thread_prefix_id": 0,
                    "last_thread_title": "Ваши предложения по названию партийного ютуб канала",
                    "message_count": 8,
                    "min_tags": 0,
                    "require_prefix": false
                }
            }
              """;
    final json = Json.fromString(jsonString);
    expect(() => Forum.fromWeb(json), throwsA(Forum.errorIsDamaged));
  });
}
