import 'package:flutter_test/flutter_test.dart';

import 'package:d7forum_api/d7forum_api.dart';

void main() {
  test('from JSON', () {
    final jsonString = """
  {
        "current_page": 1,
        "last_page": 1,
        "per_page": 20,
        "shown": 3,
        "total": 3
    }
  """;
    final json = Json.fromString(jsonString);
    final item = Pagination.fromWeb(json);
    expect(item.isDamaged(), false);

    expect(item.currentPage, 1);
    expect(item.lastPage, 1);
    expect(item.perPage, 20);
    expect(item.shown, 3);
    expect(item.total, 3);
  });

  test('from bad json', () {
    final jsonString = """
  {
        "current_pa
  """;
    final json = Json.fromString(jsonString);
    expect(() => Pagination.fromWeb(json), throwsA(Pagination.errorIsDamaged));
  });

  test('field missing', () {
    final jsonString = """
  {
        "current_page": 1,
        "last_page": 1,
        "shown": 3,
        "total": 3
    }
  """;
    final json = Json.fromString(jsonString);
    expect(() => Pagination.fromWeb(json), throwsA(Pagination.errorIsDamaged));
  });
}
